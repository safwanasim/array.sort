# Higher Order Functions in JavaScript

## Functions in javascript
Functions are the building blocks of JavaScript programming. They give us a way to shape large programs, to reduce code repitition, improve code reusability and perform various calculations/tasks with them.

Functions in JavaScript are first-class citizens because they can be treated like any variable. Functions have their own datatype, and hence, they can be assigned to variables as values, but are different from all other data types because they can be invoked. 
```js
const square = function(x) {
  return x * x;
};

//f is assigned a function called square
let f = square; 

square(10); //100

//Since f has a function value, it can be invoked.
f(10); // 100 
```
## Higher-Order Functions
Higher-Order Functions are functions that operate on other functions - they can take functions as arguments and return them. It's higher-order because instead of operating on strings, numbers, or booleans, it goes higher to operate on functions. 

## Map()
The `.map()` method takes in a callback function and applies that callback function on every element in the array. It then returns a new array with the results of applying the callback function on every element of original array. 

**Syntax of JavaScript Map()**
```js 
array.map(function(currentValue, index, array)) 
```
**Parameters:**
1.  `function`  - Required. A function or search criteria that would be used to filter values in the array
	-   `currentValue`  - Required, the current element that needs to be filtered
	-   `index`  - Optional, in case you would like to start the iteration from a specific index
	-   `arr`  - The array object being called upon
	
3.  `thisValue`  - Optional, this value present would be passed in case the parameter is empty

**Example 1** -- **Invoking a callback function on each item in an array**

`.map()` takes in a callback function as an argument, the callback function takes in the current value of the item being processed by the function and then it returns the transformed value based value.
```js
const originalArray = [3,5,15,25];

//pass a function to map that multiplies each element by 3
const resultOfMap = originalArray.map(x => x*3);

console.log(resultOfMap);
//expected output: Array [9,15,45,75]
```

**Example 2**  - **Iterating an array of objects and then returning a new with desired values from object**

`.map()`  can be used to iterate through objects in an array and, in a similar fashion to traditional arrays, modify the content of each individual object and return a new array. This modification is done based on what is returned in the callback function. 
```js
const companies= [
  {name: "Company One", category: "Finance", start: 1981, end: 2004},
  {name: "Company Two", category: "Retail", start: 1992, end: 2008},
  {name: "Company Three", category: "Auto", start: 1999, end: 2007},
  {name: "Company Four", category: "Retail", start: 1989, end: 2010},
  {name: "Company Five", category: "Technology", start: 2009, end: 2014},
  {name: "Company Six", category: "Finance", start: 1987, end: 2010},
  {name: "Company Seven", category: "Auto", start: 1986, end: 1996},
  {name: "Company Eight", category: "Technology", start: 2011, end: 2016},
  {name: "Company Nine", category: "Retail", start: 1981, end: 1989}
];

 //Create array of company names
const companyNames = companies.map(function(company) {
	return company.name
});

//Create an array of company names along with the time spent at each company
const testMap = companies.map(function(company) {
	return `${company.name} [${company.start} - ${company.end}]`;
});
```
## Filter()
The `filter()` method tests the elements of the original array, and only returns the values which pass the test condition. The search crtieria in the JavaScript filter function is passed in a `callbackfn`.

**Syntax of JavaScript filter():**
```js
array.filter(function(currentValue, index, arr), thisValue)
```
Here, `array` refers to the original array. 
   
**Parameters:**
1.  `function`  - Required. A function or search criteria that would be used to filter values in the array
	-   `currentValue`  - Required, the current element that needs to be filtered
	-   `index`  - Optional, in case you would like to start the iteration from a specific index
	-   `arr`  - The array object being called upon
	
3.  `thisValue`  - Optional, this value present would be passed in case the parameter is empty

**Example 1** -- **Using Filter on an array of objects**

`.filter()` takes in a callback function as an argument, the callback function takes in the current value of the item being processed by the function and returns the value if the callback function is satisfied.
```js
var animals = [
  {name: "Bats", type: "mammal"},
  {name: "Elephant", habitat: "mammal"},
  {name: "Squirrels", habitat: "mammal"},
  {name: "Owl", habitat: "non-mammal"},
  {name: "Penguin", habitat: "non-mammal"}
];
var mammals =  creatures.filter(function(animal) {
  return animal.type == "mammal";
});
console.log(mammals);
```
This function filters out a list of mammals and returns a new array that only has animals of the type mammals.

**Example 2** -- **Using Filter on an array of prime numbers**

The following example returns all prime numbers in the array:

```js
const array = [-3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];

function isPrime(num) {
  for (let i = 2; num > i; i++) {
    if (num % i == 0) {
      return false;
    }
  }
  return num > 1;
}

console.log(array.filter(isPrime)); // [2, 3, 5, 7, 11, 13]
```
The filter method takes in a callback and filters elements from the original array based on the callback return value


## Reduce()
The `reduce()` function in javascript  method executes a `reducer` function (that you provide) on each element of the array, resulting in single output value. The reduce function enables us to get the  single value as return when passing an array, thus, in a way, grouping  the elements. 

### reduce() Syntax
The syntax of the  `reduce()`  method is:
```js
arr.reduce(callback(accumulator, currentValue), initialValue)
```
Here,  arr  is an array.

### reduce() Parameters

The  ```reduce()```  method takes in:

1. callback  - The function to execute on each array element (except the first element if no  initialValue  is provided). It takes in
    -   accumulator  - It accumulates the callback's return values.
    -   currentValue  - The current element being passed from the array.
 2.  initialValue  (optional) - A value that will be passed to  `callback()`  on first call. If not provided, the first element acts as the  accumulator  on the first call and  `callback()`  won't execute on it.

**Example 1** -- **Using reduce on an array of objects**

Javascript array  `reduce()`  method applies a function simultaneously against two values of the array (from left-to-right) as to reduce it to a single value.
```js
const items = [
  { name: 'Fish', price: 300 },
  { name: 'Prawns', price: 305 },
  { name: 'Chicken', price: 150 },
  { name: 'Mutton', price: 400 },
]
const totalPrice = items.reduce((total, item) => {
  return total + item.price
}, 0)
console.log(totalPrice) // 1155

// This can also be written as follows
const reducer = function(total, item) {
  return total + item.price
}
const totalPrice = items.reduce(reducer, 0)
console.log(totalPrice) // 1155
```
Here, reduce function calculates the total value price of the items in the object, thus reducing the array of objects to a single value, the following table demonstrates the values of total, item.price at every iteration

| Iteration | total | item.price | return |
|-----------|-------|------------|--------|
| 1         | 0     | 300        | 300    |
| 2         | 300   | 305        | 605    |
| 3         | 605   | 150        | 755    |
| 4         | 755   | 400        | 1155   |

**Example 2:**
Grouping Objects by a property
```js
let people = [
  { name: 'Max', age: 24 },
  { name: 'Verstappen', age: 24 },
  { name: 'Hamilton', age: 36 }
];

function groupBy(objectsArray, property) {
  return objectArray.reduce(function (total, obj) {
    let key = obj[property]
    if (!total[key]) {
      total[key] = []
    }
    total[key].push(obj)
    return total
  }, {})
}

let groupedPeople = groupBy(people, 'age')
// groupedPeople is:
// {
//   24: [
//     { name: 'Max', age: 24 },
//     { name: 'Verstappen', age: 24 }
//   ],
//   21: [{ name: 'Hamilton', age: 36 }]
// }
```

## Chaining Higher-Order Functions
HIgher Order function is a function that takes functions as arguments and returns functions as output. For example,`Array.prototype.map`,  `Array.prototype.filter`  and  `Array.prototype.reduce` are some Higher-Order functions built into the JavaScript language.

**Function Chaining:** 
Function chaining is a pattern in JavaScript where many functions are invoked on a single object. Each function call returns an object. This allows the functions to be chained together in a single statement, without requiring variables to store the result of every function call. Function chaining simplifies the code.

**Example 1**

```js 
const addTwoValues = (a, b) => a + b;
const checkIsEven = num => num % 2 === 0;

const data = [2, 3, 1, 5, 4, 6];

const evenValues = data.filter(isEven); // [2, 4, 6]
const evenSum = evenValues.reduce(add); // 12
```
As you can see in  the example above, we have two functions, `addTwoValues` - adds two numbers, and, `checkIsEven` - checks if the number is even. 

The purpose of the above program is to calculate the sum of even numbers in the array. So, first we call `Array.filter()` on the given data array, with the function `isEven` as callback, second, we call `Array.reduce()` on the array returned by first step and pass in the `add` function as callback. As we can see, this is a two step process, where the results of every step is stored in a variable and thus, it makes the task cumbersome, although there is no denying that the code is more readable, but for a developer storing the result of every step is a cumbersome task.

**Solving the above problem using chaining**
```js
const addTwoValues = (a, b) => a + b;
const checkIsEven = num => num % 2 === 0;

const data = [2, 3, 1, 5, 4, 6];
const evenSum = data.filter(checkIsEven).reduce(addTwoValues,0); // 12 
```
The sum of even values can be solved in a single step, as shown above.  `Array.filter(checkIsEven)` is chained to `Array.reduce(addTwoValues)` such that the output of `Array.filter(checkIsEven)` acts as an input for `Array.reduce(addTwoValues)`. This is called function chaining. This saves us the task of storing the result of the first step. 

**Example 2**

Chaining `reduce()` and `filter()` to get the price of all vegetables in a grocery list
```js
const grocery = [
  { unit: "rice", price: 20 },
  { unit: "Beetroot", price: 30 },
  { unit: "pulse", price: 30 },
  { unit: "Capsicum", price: 40 },
  { unit: "onion", price: 50 },
  { unit: "wheat", price: 60 },
];
let vegetables = ['Beetroot', 'onion', 'Capsicum']
//an array of vegetables for reference

const isVeggie = (unit) => unit.vegetables === true //returns true if item is a vegetable
const totalPrice = (total,curr) => return total+curr.price //calculating total price of vegetables

let result = grocery.filter(isVeggie).reduce(totalPrice,0)
```
In the above example, the `Array.filter()` method filters all vegetables and passes it to the `Array.reduce()` function which calculates the price of all vegetables

 **Example 3**

Chaining `Map(), reduce(), filter()` & `sort()` to manage data.

The task is to filter data according to active users, convert date to DD/MM/YYYY, sort data according to date and then calculate total balance
```js
 const data = [
 {isActive:false, balance: 3,513.91, registered:  "2016-09-26T06:30:35 -06:-30"},
 {isActive:true, balance: 4,913.91, registered:  "2017-09-26T06:30:35 -06:-30"},
 {isActive:true, balance: 5,983.91, registered:  "2019-09-26T06:30:35 -06:-30"},
 {isActive:false, balance: 6,193.91, registered:  "2020-09-26T06:30:35 -06:-30"},
 {isActive:true, balance: 7,673.91, registered:  "2008-09-26T06:30:35 -06:-30"},
 {isActive:true, balance: 8,873.91, registered:  "2070-09-26T06:30:35 -06:-30"},
 ];

const activeCheck = (elem) => elem.isActive == true //returns if isActive is true
const dateConvert = (data) => {
	let date = data.split("T")[0].split("-")
	return `${date[2]}/${date[1]}/${date[0]}`
}
const totalBalance = (total,curr) => return total + curr.balance
const sortByDate = (a,b) => return a.registered - b.registered

let result = data.filter(activeCheck)
				      .map((elem) => {elem.registered = dateConvert(elem.registered);
						return elem;
					}).sort(sortByDate)
					.reduce(totalBalance,0)
```

The above example highlights the usage of chaining. We first use  `Array.filter()`  on data to get back a list of active users, then we use  `Array.map()`  to convert the registered date field to the format DD/MM/YYYY, once the conversion is done, we sort the object according to the date property, before using  `Array.reduce()`  to get the totalBalance of active users.

Notice how we did not have to store the result of each step or each function call. We take the returned value of one function as an argument to the next, this is function-chaining, where the functions are chained together and all the operations are performed on a single object.
